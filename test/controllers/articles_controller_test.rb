require './test/test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get articles_url
    assert_response :success
  end

    test "should create article" do
      assert_difference('Article.count') do
        post articles_url, params: { article: {  title: 'Hello Railsss', text: 'Rails is awesome!' } }, headers: { Authorization: ActionController::HttpAuthentication::Basic.encode_credentials('dhh', 'secret') }
      end

      assert_redirected_to article_path(Article.last)
    end

  test "Average rating is 3.5" do
    assert_equal(articles(:one).ratings_average, 3.5,"Average rating is 3.5")
  end

end
