require 'csv'
class Article < ApplicationRecord
  has_many :comments, dependent: :destroy
  has_many :ratings, dependent: :destroy
  validates :title, presence: true,
            length: { minimum: 5 }
  validates :text, presence: true,length: { minimum: 5 }
  # RATINGS
  def ratings_average
    total_count = 0
    ratings.each do |rating|
      total_count = total_count + rating.score
    end
    if ratings.length >0
      total_count = total_count/ratings.length
    end
    total_count.round(1)
  end

  def topRating
    if ratings.length >0
      top_rating=  ratings.order('ratings.score ASC').last.score
      top_rating.round(1)
    else
      top_rating=0
    end
    top_rating
  end
  def minRating
    if ratings.length >0
      minRating=  ratings.order('ratings.score ASC').first.score
      minRating.round(1)
    else
      minRating=0
    end
    minRating
  end

# COMMENTS
  def comments_average
    total_caract_count = 0
    comments.each do |comment|
      total_caract_count = total_caract_count + comment.body.length
    end
    if ratings.length >0
      total_caract_count = total_caract_count/comments.length
    end
    total_caract_count.round(1)
  end

  #CSV
  def self.to_csv
    attributes = %w{title text minRating topRating ratings_average comments comments_average}

    CSV.generate(headers: true) do |csv|
      csv << attributes
      all.each do |article|
        csv << attributes.map{ |attr|
          if attr=='comments'
            article.send(attr).length
          else
            article.send(attr)
          end
          }

      end
    end
  end
end
